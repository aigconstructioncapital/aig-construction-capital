AIG Construction Capital is a leading resource for Construction Companies in New York by providing access to Invoice Factoring and Purchase Order Financing. Once approved, funding will be available to your construction company in as little as 24 hours.

Address: 1410 Broadway, #3203, New York, NY 10018, USA

Phone: 212-847-1347

Website: https://www.aigccapital.com
